# Tool Assisted Speedrun

## State of the art

![TasBot](./images/tasbot.jpg)

---
name: main_screen

# The TAS

## Alias Tool Assisted Speedrun

![Nahoc](./images/nahoc.png)
![Nitrofski](./images/nitrofski.png)

---
name: summary

.center[.title[Summary]]

<hr />

### Speedrun : Présentation
### Tool Assisted Speedrun : Présentation
### Tool Assisted Speedrun : Techniques avancées

---
name: speedrun-presentation

.center[.title[Speedrun]]

<hr />

### Présentation

![T-Shirt](./images/t-shirt-speedrun.jpg)

---
name: speedrun-finalite

.center[.title[Speedrun]]

<hr />

### Finalité d'un speedrun

Il faut terminer un jeu le plus rapidement possible.

* Préciser le jeu
* Préciser la console
* Préciser la personne qui a speedrunné
* Préciser la catégorie du speedrun
* Préciser la version du jeu (facultatif)
* Préciser le temps final

---
name: speedrun-categorie

.center[.title[Speedrun]]

<hr />

### Catégorie

* Low% / Any% / 100% (voire plus)
* Glitchless / Warpless
* New Game +
* Exotiques (RBO, Contraintes, Maximum Score, ...)

---
name: speedrun-environnement

.center[.title[Speedrun]]

<hr />

### Environnement

* Une console
* Une manette
* Un jeu
* Un chrono (facultatif si le jeu précise l'IGT)
* Twitch (optionnel)

---
name: speedrun-visibilite

.center[.title[Speedrun]]

<hr />

### Visibilité

* Classement : speedrun.com
* Direct : speedrunslive.com
* Portail : speeddemosarchive.com

---
name: speedrun-competences

.center[.title[Speedrun]]

<hr />

### Compétences requises

* Entraînement
* Endurance
* Garder son sang froid
* Un brin de folie (speedrun à l'aveugle)

---
name: speedrun-conventions

.center[.title[Speedrun]]

<hr />

### Conventions

* SGDQ / AGDQ
* PAX
* ESA

![CTR-AGDQ](./images/crash-team-racing-agdq.jpg)

---
name: speedrun-elsewhere

.center[.title[Speedrun]]

<hr />

### Elsewhere

* Summoning Salt (history)

---
name: tool-assisted-speedrun-presentation

.center[.title[Tool Assisted Speedrun]]

<hr />

### Now, TAS

Après les speedruns, allons côtoyer le côté Tool Assisted

.center[<iframe width="560" height="315" src="https://www.youtube.com/watch?v=0mregEW6kVU" frameborder="0" allowfullscreen></iframe>]

---
name: tool-assisted-speedrun-environnement

.center[.title[Tool Assisted Speedrun]]

<hr />

### Environnement

* Un PC (Ok pour Windows / Linux / Mac mais avec des différences)
* Un émulateur (Dolphin pour GC, lsnes pour la SNES, BizHawk pour beaucoup de consoles, ...)
* La ROM du jeu

---
name: tool-assisted-speedrun-tools

.center[.title[Tool Assisted Speedrun]]

<hr />

### Les outils dans les émulateurs

* RAM Search / RAM Watch
* Rerecord (ou Save State)
* Frame Advanced / Slow Motion
* Hex Editing

---
name: tool-assisted-speedrun-lua

.center[.title[Tool Assisted Speedrun]]

<hr />

### Lua

* Afficher des informations sur l'écran de jeu
* Exécuter du code (série de tests par exemple)

---
name: tool-assisted-speedrun-skills

.center[.title[Tool Assisted Speedrun]]

<hr />

### Compétences requises

* Esprit créatif
* Avoir beaucoup de temps à soi

---
name: tool-assisted-speedrun-references

.center[.title[Tool Assisted Speedrun]]

<hr />

### Les références

* tasvideos.org
* Youtube / Nicovideo
* 88 miles à l'heure / Speed Game

---
name: tool-assisted-speedrun-tasvideos

.center[.title[Tool Assisted Speedrun]]

<hr />

### Le site tasvideos.org

* Portail (News / Forum / Articles / Ressources / ...)
* Sections (Submissions / Publications / Vault / Award)
* Mentions (Star / Moon / Notable Improvement)
* Deux chaînes Youtube : TASVideosChannel et WebNations

---
name: tool-assisted-speedrun-lost-frame

.center[.title[Tool Assisted Speedrun]]

<hr />

### À la recherche de la frame perdue

![Indiana Jones](./images/indiana-jones.jpg)

---
name: tool-assisted-speedrun-ordinary-tactics

.center[.title[Tool Assisted Speedrun]]

<hr />

### Les techniques ordinaires

* Routing
* Menuing
* Jouer en japonais
* Réduction des lags
* Sequence Break

---
name: tool-assisted-speedrun-advanced-tactics

.center[.title[Tool Assisted Speedrun]]

<hr />

### Les techniques avancées

* Take damage to save time
* Damage boost
* Death Warp / Death Abuse
* Recherche de glitch / Out Of Bound (OOB)
* Pause buffering

---
name: tool-assisted-speedrun-programmer-tactics

.center[.title[Tool Assisted Speedrun]]

<hr />

### Corruption de mémoire (Game End Glitch)

.center[<iframe width="560" height="315" src="https://www.youtube.com/watch?v=FkQdwUns7H8" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>]

---
name: tool-assisted-speedrun-prefered-heart

.center[.title[Tool Assisted Speedrun]]

<hr />

### Préféré (de coeur)

.center[<iframe width="560" height="315" src="https://www.youtube.com/watch?v=_PDgGALMjP0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>]

---
name: tool-assisted-speedrun-prefered-impressive

.center[.title[Tool Assisted Speedrun]]

<hr />

### Préféré 2

.center[<iframe width="560" height="315" src="https://www.youtube.com/watch?v=W9gxFkOz2_4" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>]

---
name: tool-assisted-speedrun-prefered-skills

.center[.title[Tool Assisted Speedrun]]

<hr />

### Préféré 3

.center[<iframe width="560" height="315" src="https://www.youtube.com/watch?v=LDx4KpYdykg" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>]

---
name: tool-assisted-speedrun-conclusion

.center[.title[Tool Assisted Speedrun]]

<hr />

### Conclusion

* Taser n'est pas tricher (pas de cheat, pas de rom corrompues)
* Un taseur ne fait pas d'erreur, il manipule la chance
* "Il ne fait pas bon être un boss dans un tool assisted speedrun"

![End](./images/this-is-the-end.jpg)

---
name: tool-assisted-speedrun-questions

.center[.title[Tool Assisted Speedrun]]

<hr />

### Questions ?

![FullMoonIssue](./images/Came_ha_mais_avec_queur.png)
