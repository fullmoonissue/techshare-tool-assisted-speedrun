<?php

$html = <<<HTML
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Lunch And Learn : Tool Assisted Speedrun</title>
        <style>
        .left-column {
            width: 20%;
            height: 92%;
            float: left;
        }
        .right-column {
            width: 75%;
            float: right;
            padding-top: 1em;
        }
        .split-column {
            width: 50%;
            float: left;
            padding-top: 1em;
        }
        .third-column {
            width: 33%;
            height: 82%;
            float: left;
            border-right: 1px solid #aaa;
        }
        .red {
            color: red;
        }
        .gray {
            color: gray;
        }
        .need-ref {
            border-bottom: 1px solid #aaa;
        }
        .title {
            font-weight: bold;
            border-bottom: 3px dotted #aaa;
        }
        </style>
    </head>
    <body>
        <textarea id="source">
            [MD]
        </textarea>
        <script src="./js/remarks.min.js"></script>
        <script>
            var slideshow = remark.create();
            var tables = document.querySelectorAll('table');
            for(var i in tables) {
                tables[i].setAttribute('border', '1')
                tables[i].setAttribute('cellpadding', '15')
            }
        </script>
    </body>
</html>
HTML;

file_put_contents(
    __DIR__.'/public/techshare.html',
    preg_replace(
        '{\[MD\]}',
        file_get_contents(__DIR__.'/TECHSHARE.md'),
        $html
    )
);